using System.Collections.Generic;
using support_it.Domain.Entities.Portal;

namespace support_it.Domain.Interfaces.Services.Portal
{
    public interface IDashboardAdminService : IServiceBase<DashboardAdmin>
    {
        bool Insert(string email);
    }
}