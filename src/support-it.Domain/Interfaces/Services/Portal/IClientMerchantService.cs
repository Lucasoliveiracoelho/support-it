using support_it.Domain.Entities.Portal;

namespace support_it.Domain.Interfaces.Services.Portal
{
    public interface IClientMerchantService: IServiceBase<ClientMerchant>
    {
        bool DeleteAccessEc(string email);
    }
}