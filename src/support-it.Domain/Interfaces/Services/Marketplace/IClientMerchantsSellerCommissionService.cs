using support_it.Domain.DTO.Marketplace;
using support_it.Domain.Entities.Marketplace;

namespace support_it.Domain.Interfaces.Services.Marketplace
{
    public interface IClientMerchantsSellerCommissionService: IServiceBase<ClientMerchantsSellerCommission>
    {
        ClientMerchantsSellerCommissionDTO ConsultCommission(string merchantId);
    }
}