using System.Collections.Generic;
using support_it.Domain.DTO.Pdv;
using support_it.Domain.Entities.Pdv.PdvSale;

namespace support_it.Domain.Interfaces.Services
{
    public interface ISaleService : IServiceBase<Sale>
    {
         List<SaleDTO> Search(string taxId,string netAmount);

         List<ProductDTO> SearchProducts(int saleId);
         
    }
}