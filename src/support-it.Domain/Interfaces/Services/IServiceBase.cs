namespace support_it.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> where TEntity :class
    {
         void Add(TEntity entity);
    }
}