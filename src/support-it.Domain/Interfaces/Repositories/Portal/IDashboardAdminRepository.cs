using System.Collections.Generic;
using support_it.Domain.Entities.Portal;

namespace support_it.Domain.Interfaces.Repositories.Portal
{
    public interface IDashboardAdminRepository : IRepositoryBase<DashboardAdmin>
    {
        bool Insert(string email); 
    }
}