using support_it.Domain.Entities.Portal;

namespace support_it.Domain.Interfaces.Repositories.Portal
{
    public interface IClientMerchantRepository : IRepositoryBase<ClientMerchant>
    {
        bool DeleteAccessEc(string email);
    }
}