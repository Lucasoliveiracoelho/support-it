using support_it.Domain.DTO.Marketplace;
using support_it.Domain.Entities.Marketplace;

namespace support_it.Domain.Interfaces.Repositories.Marketplace
{
    public interface IClientMerchantsSellerCommissionRepository: IRepositoryBase<ClientMerchantsSellerCommission>
    {
        ClientMerchantsSellerCommissionDTO ConsultCommission(string merchantId);
    }
}