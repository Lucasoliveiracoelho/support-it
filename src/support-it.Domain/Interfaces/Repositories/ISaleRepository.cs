using System.Collections.Generic;
using support_it.Domain.DTO.Pdv;
using support_it.Domain.Entities.Pdv.PdvSale;

namespace support_it.Domain.Interfaces.Repositories
{
    public interface ISaleRepository : IRepositoryBase<Sale>
    {
        List<SaleDTO> Search(string taxId,decimal netAmount); 

        List<ProductDTO> SearchProducts(int saleId);
    }
}