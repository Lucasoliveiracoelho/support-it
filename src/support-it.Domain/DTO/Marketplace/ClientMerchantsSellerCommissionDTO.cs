using System;

namespace support_it.Domain.DTO.Marketplace
{
    public class ClientMerchantsSellerCommissionDTO
    {
        public string ClientMerchantId { get; set; }
        public decimal CommissionAmount { get; set; }
        public DateTime DateLog { get; set; }
        public decimal TriggerAmount { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime DeactivationDate { get; set; }
    }
}