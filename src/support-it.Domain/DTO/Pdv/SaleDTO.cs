using System;

namespace support_it.Domain.DTO.Pdv
{
    public class SaleDTO
    {
       public int Id { get; set; } 
       public DateTime CreationDate { get; set; }
       public bool Completed { get; set; }
       public double TotalCashback { get; set; }
       public string TaxId { get; set; }
       public string OperationCode { get; set; }
       public string DocumentNumber { get; set; }
       public decimal NetAmount { get; set; }
       public string SaleId { get; set; }

    }
}