namespace support_it.Domain.DTO.Pdv
{
    public class ProductDTO
    {
        public int Id { get; set; }

        public int SaleId { get; set; }
        public string Barcode { get; set; }
        public string Description { get; set; }
        public double NetPrice { get; set; }
        public double PercentualCashback { get; set; }
        public double Cashback { get; set; }
        public double CashbackWithFactor { get; set; }
        public double Quantity { get; set; }
    }
}