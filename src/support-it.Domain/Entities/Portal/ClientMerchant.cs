using System.ComponentModel.DataAnnotations.Schema;

namespace support_it.Domain.Entities.Portal
{
    [Table("client_merchants")]
    public class ClientMerchant
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("tax_id")]
        public string TaxId { get; set; }
        [Column("password")]
        public string Password { get; set; }

    }
}