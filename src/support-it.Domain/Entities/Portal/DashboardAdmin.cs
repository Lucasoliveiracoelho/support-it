using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace support_it.Domain.Entities.Portal
{
    [Table("dashboard_admins")]
    public class DashboardAdmin
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("username")]        
        public string Username { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("active")]
        public bool Active { get; set; }

        [Column("updated_at")]
        public DateTime Updated { get; set; }

        [Column("created_at")]
        public DateTime Created { get; set; }
        
    }
}