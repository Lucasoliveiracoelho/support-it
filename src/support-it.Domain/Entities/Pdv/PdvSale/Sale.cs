
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace support_it.Domain.Entities.Pdv.PdvSale
{
    [Table("sale")]
    public class Sale
    {

       [Column("id")]
       public int Id { get; set; } 

       [Column("created_by")]
       public string CreatedBy { get; set; }

       [Column("creation_date")]
       public DateTime CreationDate { get; set; }

       [Column("completed")]
       public bool Completed { get; set; }

       [Column("total_cashback")]
       public double TotalCashback { get; set; }

       [Column("tax_id")]
       public string TaxId { get; set; }

       [Column("operation_code")]
       public string OperationCode { get; set; }

       [Column("document_number")]
       public string DocumentNumber { get; set; }

       [Column("net_amount")]
       public decimal NetAmount { get; set; }

       [Column("sale_id")]
       public string SaleId { get; set; }
    }
}