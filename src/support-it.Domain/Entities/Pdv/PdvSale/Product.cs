using System.ComponentModel.DataAnnotations.Schema;

namespace support_it.Domain.Entities.Pdv.PdvSale
{
    [Table("product")]
    public class Product
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("sale_id")]
        public int SaleId { get; set; }

        [Column("barcode")]
        public string Barcode { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("net_price")]
        public double NetPrice { get; set; }

        [Column("percentual_cashback")]
        public double PercentualCashback { get; set; }

        [Column("cashback")]
        public double Cashback { get; set; }

        [Column("cashback_with_factor_of_reduction")]
        public double CashbackWithFactor { get; set; }

        [Column("quantity")]
        public double Quantity { get; set; }

    }
}