using System;

namespace support_it.Domain.Entities.Marketplace
{
    public class ClientMerchantsSellerCommission
    {
        public string ClientMerchantId { get; set; }
        public decimal CommissionAmount { get; set; }
        public DateTime DateLog { get; set; }
        public decimal TriggerAmount { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime DeactivationDate { get; set; }
    }
}