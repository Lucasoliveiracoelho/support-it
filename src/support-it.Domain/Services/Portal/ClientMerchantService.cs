using System;
using support_it.Domain.Entities.Portal;
using support_it.Domain.Interfaces.Repositories;
using support_it.Domain.Interfaces.Repositories.Portal;
using support_it.Domain.Interfaces.Services.Portal;

namespace support_it.Domain.Services.Portal
{
    public class ClientMerchantService : ServiceBase<ClientMerchant>, IClientMerchantService
    {
        private readonly IClientMerchantRepository _clientMerchantRepository;
        public ClientMerchantService(IClientMerchantRepository clientMerchantRepository) : base(clientMerchantRepository)
        {
            _clientMerchantRepository = clientMerchantRepository;
        }

        public bool DeleteAccessEc(string email)
        {
            try
            {
                if(email !="" && !string.IsNullOrEmpty(email))
                {
                    return _clientMerchantRepository.DeleteAccessEc(email);
                }else
                {
                    throw new Exception();
                }
            }
            catch(Exception exc)
            {
                throw;
            }
        }
    }
}