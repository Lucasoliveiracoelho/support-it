using System;
using System.Collections.Generic;
using support_it.Domain.Entities.Portal;
using support_it.Domain.Interfaces.Repositories;
using support_it.Domain.Interfaces.Repositories.Portal;
using support_it.Domain.Interfaces.Services.Portal;

namespace support_it.Domain.Services.Portal
{
    public class DashboardAdminService : ServiceBase<DashboardAdmin>, IDashboardAdminService
    {
        private readonly IDashboardAdminRepository _dashboardAdminRepository;
        public DashboardAdminService(IDashboardAdminRepository dashboardAdminRepository) : base(dashboardAdminRepository)
        {
            _dashboardAdminRepository = dashboardAdminRepository;
        }
        public bool Insert(string email)
        {
            try
            {
                var splitEmail = email.Split('@');

                return _dashboardAdminRepository.Insert(splitEmail[1]);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.InnerException.Message);
            }
        }
    }
}