using support_it.Domain.DTO.Marketplace;
using support_it.Domain.Entities.Marketplace;
using support_it.Domain.Interfaces.Repositories;
using support_it.Domain.Interfaces.Services.Marketplace;

namespace support_it.Domain.Services.Marketplace
{
    public class ClientMerchantsSellerCommissionService : ServiceBase<ClientMerchantsSellerCommission>, IClientMerchantsSellerCommissionService
    {
        public ClientMerchantsSellerCommissionService(IRepositoryBase<ClientMerchantsSellerCommission> repositoryBase) : base(repositoryBase)
        {
        }

        public ClientMerchantsSellerCommissionDTO ConsultCommission(string merchantId)
        {
            throw new System.NotImplementedException();
        }
    }
}