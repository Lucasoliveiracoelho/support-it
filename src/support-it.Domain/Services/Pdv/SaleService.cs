using support_it.Domain.Entities;
using support_it.Domain.Interfaces.Services;
using support_it.Domain.Entities.Pdv.PdvSale;
using support_it.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using support_it.Domain.DTO.Pdv;
using System;

namespace support_it.Domain.Services.Pdv
{
    public class SaleService : ServiceBase<Sale>, ISaleService
    {
        private readonly ISaleRepository _saleRepository;
        public SaleService(ISaleRepository saleRepository):base(saleRepository)
        {   
            _saleRepository = saleRepository;
        }
        public List<SaleDTO> Search(string taxId, string netAmount)
        {
            try
            {
                if(taxId == null)
                {
                    throw new Exception("PREENCHA O CPF!");
                }
                if(netAmount == null)
                {
                    throw new Exception("PREENCHA O VALOR!");
                }
                var conNetAmount = decimal.Parse(netAmount);
                var listSale = _saleRepository.Search(taxId,conNetAmount);
                if(listSale.Count <= 0)
                {
                    throw new Exception("NÃO EXISTE NENHUMA TRANSAÇÃO!");
                }
                return listSale;
            }
            catch(Exception ex)
            {
                throw new Exception("error", ex);
            }

        }

        public List<ProductDTO> SearchProducts(int saleId)
        {
            return _saleRepository.SearchProducts(saleId);
        }
    }
}