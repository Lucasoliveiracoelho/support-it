using support_it.Domain.Interfaces.Repositories;
using support_it.Domain.Interfaces.Services;

namespace support_it.Domain.Services
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repositoryBase;

        public ServiceBase(IRepositoryBase<TEntity> repositoryBase)
        {
            _repositoryBase = repositoryBase;
        }

        public void Add(TEntity entity) =>
            _repositoryBase.Add(entity);
    }
}