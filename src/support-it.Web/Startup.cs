﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using support_it.Data.Context;
using support_it.Domain.Services.Pdv;
using support_it.Domain.Interfaces.Services;
using support_it.Domain.Interfaces.Repositories;
using support_it.Data.Repositories.Pdv;
using support_it.Data.Context.Portal;
using support_it.Domain.Services.Portal;
using support_it.Domain.Interfaces.Services.Portal;
using support_it.Data.Repositories.Portal;
using support_it.Domain.Interfaces.Repositories.Portal;
using support_it.Data.Context.Marketplace;
using support_it.Domain.Interfaces.Repositories.Marketplace;
using support_it.Data.Repositories.Marketplace;
using support_it.Domain.Interfaces.Services.Marketplace;
using support_it.Domain.Services.Marketplace;

namespace support_it.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddEntityFrameworkNpgsql().AddDbContext<PdvDbContext>(opt=>
            // opt.UseNpgsql(Configuration.GetConnectionString("PdvConnection")));

            // services.AddDbContext<PdvDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<PortalDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("PortalConnection")));
            services.AddDbContext<MarketplaceDbContext>(options=>options.UseSqlServer(Configuration.GetConnectionString("MarketplaceConnection")));

            //registrar serviço
            services.AddTransient<ISaleRepository,SaleRepository>();
            services.AddTransient<ISaleService,SaleService>();

            services.AddTransient<IClientMerchantsSellerCommissionRepository, ClientMerchantsSellerCommissionRepository>();
            services.AddTransient<IClientMerchantsSellerCommissionService,ClientMerchantsSellerCommissionService>();

            services.AddTransient<IDashboardAdminRepository,DashboardAdminRepository>();
            services.AddTransient<IDashboardAdminService,DashboardAdminService>();

            services.AddTransient<IClientMerchantService, ClientMerchantService>();
            services.AddTransient<IClientMerchantRepository,ClientMerchantRepository>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
