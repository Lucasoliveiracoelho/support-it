using System;
using Microsoft.AspNetCore.Mvc;
using support_it.Domain.Interfaces.Services;
using support_it.Domain.Interfaces.Services.Portal;
using support_it.Domain.Services.Portal;

namespace support_it.Web.Controllers
{
    public class PortalController :Controller
    {
        readonly IDashboardAdminService _dashboardAdminService;
        readonly IClientMerchantService _clientMerchantService;


        public PortalController(IDashboardAdminService dashboardAdminService, IClientMerchantService clientmMerchantService)
        {
            _dashboardAdminService = dashboardAdminService;
            _clientMerchantService = clientmMerchantService;

        }

        [HttpGet]
        public IActionResult AccessPortal()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AccessEc()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert(string email)
        {
            try
            {
                var result = _dashboardAdminService.Insert(email);
                return Json(result);
            }
            catch(Exception ex)
            {
                throw new Exception();
            }

        }
        [HttpPost]
        public ActionResult DeleteAccessEc(string email)
        {
            try
            {
                var result =_clientMerchantService.DeleteAccessEc(email);
                return Json(result);
            }
            catch(Exception ex)
            {
                return Json("");
            }

        }
    }
}