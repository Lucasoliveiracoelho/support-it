using Microsoft.AspNetCore.Mvc;
using support_it.Domain.DTO.Pdv;
using support_it.Domain.Interfaces.Services;
using support_it.Domain.Services.Pdv;
using support_it.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;

namespace support_it.Web.Controllers
{
    public class PdvController : Controller
    {
        readonly ISaleService _saleService;

        public PdvController(ISaleService saleService)
        {
            _saleService = saleService;
        }
        
        [HttpGet]
        public IActionResult Consult()
        {
            ViewData["Message"] = "Your application description page.";
            var model = new List<SaleDTO>();
            return View();
        }

        [HttpPost]
        public ActionResult Search(string taxId,string netAmount)
        {
            try
            {
                var listSale  = _saleService.Search(taxId,netAmount);
                return View("_ConsultPartialView",listSale);
            }
            catch(Exception ex)
            {
                    var men = ex.InnerException.Message;
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Content(men);
            }

        }

        [HttpPost]
        public JsonResult SearchProducts(string saleId)
        {
            var conSaleId = int.Parse(saleId);
            var listProduct = _saleService.SearchProducts(conSaleId);
            return Json(listProduct);
        }
        public ActionResult Insert()
        {
            return View();
        }

    }
}