using support_it.Data.Context;
using support_it.Domain.DTO.Marketplace;
using support_it.Domain.Entities.Marketplace;
using support_it.Domain.Interfaces.Repositories.Marketplace;

namespace support_it.Data.Repositories.Marketplace
{
    public class ClientMerchantsSellerCommissionRepository : RepositoryBase<ClientMerchantsSellerCommission>, IClientMerchantsSellerCommissionRepository
    {
        public ClientMerchantsSellerCommissionRepository(PdvDbContext pdvDbContext) : base(pdvDbContext)
        {
        }

        public ClientMerchantsSellerCommissionDTO ConsultCommission(string merchantId)
        {
            throw new System.NotImplementedException();
        }

        public bool DisableCampaignCent(string email)
        {
            throw new System.NotImplementedException();
        }
    }
}