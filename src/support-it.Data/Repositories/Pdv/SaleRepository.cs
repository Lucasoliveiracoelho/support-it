using System.Collections.Generic;
using System.Linq;
using support_it.Data.Context;
using support_it.Domain.DTO.Pdv;
using support_it.Domain.Entities.Pdv.PdvSale;
using support_it.Domain.Interfaces.Repositories;

namespace support_it.Data.Repositories.Pdv
{
    public class SaleRepository : RepositoryBase<Sale>, ISaleRepository
    {
        public SaleRepository(PdvDbContext context) : base(context)
        {            
        }
        int paginaAtual = 1;
        int itensPagina =10;
        public List<SaleDTO> Search(string taxId, decimal netAmount)
        {
            var listSale =  (from u in _pdvDbContext.Sale
                    where (u.TaxId == taxId && u.NetAmount == netAmount)
                    select new SaleDTO()
                    {
                        Id = u.Id,
                        CreationDate = u.CreationDate,
                        Completed = u.Completed,
                        TaxId = u.TaxId,
                        OperationCode = u.OperationCode,
                        DocumentNumber = u.DocumentNumber,
                        TotalCashback = u.TotalCashback,
                        NetAmount = u.NetAmount,
                        SaleId = u.SaleId,

                    }).ToList();
            return listSale;
        }
        public List<ProductDTO> SearchProducts(int saleId)
        {
            var listSale =  (from u in _pdvDbContext.Product
                    where (u.SaleId == saleId)
                    select new ProductDTO()
                    {
                        Id = u.Id,
                        Barcode = u.Barcode,
                        Description = u.Description,
                        SaleId = u.SaleId,
                        NetPrice = u.NetPrice,
                        PercentualCashback = u.PercentualCashback,
                        Cashback = u.Cashback,
                        CashbackWithFactor = u.CashbackWithFactor,
                        Quantity = u.Quantity
                    }).ToList();
            return listSale;
        }
    }
}