using Microsoft.EntityFrameworkCore;
using support_it.Data.Context;
using support_it.Data.Context.Marketplace;
using support_it.Data.Context.Portal;
using support_it.Domain.Interfaces.Repositories;

namespace support_it.Data.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly MarketplaceDbContext _marketplaceContext;
        protected readonly PdvDbContext _pdvDbContext;
        protected readonly PortalDbContext _portalDbContext;
        private readonly DbSet<TEntity> _dbSet;

        public RepositoryBase(PdvDbContext pdvDbContext)
        {
            _pdvDbContext = pdvDbContext;
            _dbSet = _pdvDbContext.Set<TEntity>();
        }
        public RepositoryBase(PortalDbContext portalDbContext)
        {
            _portalDbContext = portalDbContext;
            _dbSet = _portalDbContext.Set<TEntity>();
        }
        public RepositoryBase(MarketplaceDbContext marketplaceDbContext)
        {
            _marketplaceContext = marketplaceDbContext;
            _dbSet = _portalDbContext.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }
    }
}