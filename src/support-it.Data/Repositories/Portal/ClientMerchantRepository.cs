using support_it.Data.Context.Portal;
using support_it.Domain.Entities.Portal;
using support_it.Domain.Interfaces.Repositories.Portal;
using System.Linq;


namespace support_it.Data.Repositories.Portal
{
    public class ClientMerchantRepository :RepositoryBase<ClientMerchant>, IClientMerchantRepository
    {
        public ClientMerchantRepository(PortalDbContext contextt) : base(contextt)
        {
        }

        public bool DeleteAccessEc(string email)
        {
            var ret =  _portalDbContext.ClientMerchant.Where(x=>x.Email == email).Single();
            _portalDbContext.ClientMerchant.Remove(ret);
            _portalDbContext.SaveChanges();
            return true;
        }
    }
}