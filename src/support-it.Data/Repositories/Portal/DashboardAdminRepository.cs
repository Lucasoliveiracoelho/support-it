using System.Collections.Generic;
using support_it.Data.Context;
using support_it.Data.Context.Portal;
using support_it.Domain.Entities.Portal;
using support_it.Domain.Interfaces.Repositories.Portal;
using System.Linq;
using System;

namespace support_it.Data.Repositories.Portal
{
    public class DashboardAdminRepository : RepositoryBase<DashboardAdmin>, IDashboardAdminRepository
    {
        public DashboardAdminRepository(PortalDbContext context) : base(context)
        {
        }

        public bool Insert(string email)
        {
            var countElement = _portalDbContext.Dashboaradmin.Where(x=>x.Username == email).ToList().Count;
            if(countElement == 1)
            {
                return false;
            }else
            {
                var admin = new DashboardAdmin();
                admin.Password="$pbkdf2-sha512$25000$FcK4dw7BeE9p7Z1TSgmh1A$ok6h.fU18iZxzUY/KeZkX71ZV5EjmodQEGeLPIoxN7U4kXnl/EI9BbxsQuEudEYZwY11DvIQ6yOD7puwCVTiXw";
                admin.Username=email;
                admin.Active = true;
                admin.Updated = DateTime.Now;
                admin.Created = DateTime.Now;
                _portalDbContext.Dashboaradmin.Add(admin);
                _portalDbContext.SaveChanges();
                return true;

            }
        }
    }
}