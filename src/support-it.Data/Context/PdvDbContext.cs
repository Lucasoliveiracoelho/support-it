using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using support_it.Data.Configuration;
using support_it.Data.Configuration.Portal;
using support_it.Domain.Entities.Pdv.PdvSale;
using support_it.Domain.Entities.Portal;

namespace support_it.Data.Context
{
    public class PdvDbContext : DbContext
    {
        public PdvDbContext(DbContextOptions<PdvDbContext> options): base(options)
        {
        }

        public DbSet<Sale> Sale { get; set; }
        public DbSet<Product> Product { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
                modelBuilder.ApplyConfiguration(new SaleMap());            
                modelBuilder.ApplyConfiguration(new ProductMap());
        }
    }
}