using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using support_it.Data.Configuration;
using support_it.Data.Configuration.Portal;
using support_it.Domain.Entities.Pdv.PdvSale;
using support_it.Domain.Entities.Portal;

namespace support_it.Data.Context.Portal
{
    public class PortalDbContext : DbContext
    {
        public PortalDbContext(DbContextOptions<PortalDbContext> options) : base(options)
        { 
        }
        public DbSet<DashboardAdmin> Dashboaradmin { get; set; }
        public DbSet<ClientMerchant> ClientMerchant {get;set;}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
                modelBuilder.ApplyConfiguration(new DashboardAdminMap());
                modelBuilder.ApplyConfiguration(new ClientMerchantMap());

        }

    }
}