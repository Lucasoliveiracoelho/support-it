using Microsoft.EntityFrameworkCore;
using support_it.Domain.Entities.Marketplace;

namespace support_it.Data.Context.Marketplace
{
    public class MarketplaceDbContext: DbContext
    {
        public MarketplaceDbContext(DbContextOptions<MarketplaceDbContext> options) :base(options)
        {    
        }

        DbSet<ClientMerchantsSellerCommission> ClientMerchantsSellerComission {get;set;}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        } 
    }
}