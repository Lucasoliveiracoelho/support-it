using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using support_it.Domain.Entities.Pdv.PdvSale;

namespace support_it.Data.Configuration
{
    public class SaleMap : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.ToTable("sale");
            
            builder.HasKey(k => k.Id)
                .HasName("id");

            builder.Property(p => p.Id)
                .HasColumnName("id")
                .IsRequired();

            builder.Property(e => e.NetAmount)
                .HasColumnName("net_amount")
                .IsRequired();

            builder.Property(e => e.TaxId)
                .HasColumnName("tax_id")
                .IsRequired();

            builder.Property(e =>e.Completed)
                .HasColumnName("completed")
                .IsRequired();
            
            builder.Property(e =>e.CreationDate)
                .HasColumnName("creation_date")
                .IsRequired();
            
            builder.Property(e =>e.DocumentNumber)
                .HasColumnName("document_number")
                .IsRequired();
        } 
    }
}