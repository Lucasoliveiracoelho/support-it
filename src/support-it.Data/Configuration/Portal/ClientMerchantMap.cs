using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using support_it.Domain.Entities.Portal;

namespace support_it.Data.Configuration.Portal
{
    public class ClientMerchantMap :IEntityTypeConfiguration<ClientMerchant>
    {
       public void Configure(EntityTypeBuilder<ClientMerchant> builder)
        {
            builder.ToTable("client_merchants");

            builder.HasKey(p => p.Id)
                .HasName("id");

            builder.Property(e => e.Password)
                .HasColumnName("password");

            builder.Property(e => e.TaxId)
                .HasColumnName("tax_id");

            builder.Property(e => e.Email)
                .HasColumnName("email");
        }
    }
}