using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using support_it.Domain.Entities.Portal;

namespace support_it.Data.Configuration.Portal
{
    public class DashboardAdminMap : IEntityTypeConfiguration<DashboardAdmin>
    {
        public void Configure(EntityTypeBuilder<DashboardAdmin> builder)
        {
            builder.ToTable("dashboard_admins");

            builder.HasKey(p => p.Id)
                .HasName("id");

            builder.Property(e => e.Username)
                .HasColumnName("username");

            builder.Property(e => e.Password)
                .HasColumnName("password");

            builder.Property(e => e.Active)
                .HasColumnName("active");

            builder.Property(e => e.Created)
                .HasColumnName("created_at");

            builder.Property(e => e.Updated)
                .HasColumnName("updated_at");

        }
    }
}