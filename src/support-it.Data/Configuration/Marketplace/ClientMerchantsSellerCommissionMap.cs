using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using support_it.Domain.Entities.Marketplace;

namespace support_it.Data.Configuration.Marketplace
{
    public class ClientMerchantsSellerCommissionMap :IEntityTypeConfiguration<ClientMerchantsSellerCommission>
    {

        public void Configure(EntityTypeBuilder<ClientMerchantsSellerCommission> builder)
        {
            builder.ToTable("ClientMerchantsSellerCommission");

            builder.HasKey(p => p.ClientMerchantId)
                .HasName("ClientMerchantId");

            builder.Property(e => e.CommissionAmount)
                .HasColumnName("CommissionAmount");

            builder.Property(e => e.ActivationDate)
                .HasColumnName("ActivationDate");

            builder.Property(e => e.DateLog)
                .HasColumnName("DateLog");  

            builder.Property(e => e.TriggerAmount)
                .HasColumnName("TriggerAmount");  

            builder.Property(e => e.DeactivationDate)
                .HasColumnName("DeactivationDate");  
        }
    }
}