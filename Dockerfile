#========================== Builder Image ==========================
FROM microsoft/aspnetcore-build:2.0 AS builder

WORKDIR /support-it

COPY ./src/support-it.Web/*.csproj ./support-it.Web/
COPY ./src/support-it.Domain/*.csproj ./support-it.Domain/
COPY ./src/support-it.Data/*.csproj ./support-it.Data/

RUN dotnet restore ./support-it.Web/support-it.Web.csproj

COPY ./src ./src

WORKDIR /support-it/src/support-it.Web

RUN dotnet publish -c Release -o publish